.. _admcyclesmodule:

Module ``admcycles``
====================

.. automodule:: admcycles.admcycles
   :members:
   :undoc-members:
   :show-inheritance:
