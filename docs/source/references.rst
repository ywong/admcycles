.. _references:

**********
References
**********

.. [BHPSS] Y. Bae, D. Holmes, R. Pandharipande, J. Schmitt, R. Schwarz
   *Pixton's formula and Abel-Jacobi theory on the Picard stack*
   `arXiv:2004.08676 [math.AG] <https://arxiv.org/abs/2004.08676>`_

.. [BaChGeGrMo] M. Bainbridge, D. Chen, Q. Gendron, S. Grushevsky, M. Möller
   *The moduli space of multi-scale differentials*
   `arXiv:1910.13492 [math.AG] <https://arxiv.org/abs/1910.13492>`_

.. [BuRo] Alexandr Buryak, Paolo Rossi
   *Quadratic double ramification integrals and the noncommutative KdV hierarchy*
   `arXiv:1909.11617 [math.AG] <https://arxiv.org/abs/1909.11617>`_

.. [CJ18] E. Clader, F. Janda
   *Pixton's double ramification cycle relations*
   Geom. Topol. 22 (2018), no. 2, 1069–1108.

.. [CoMoZa-a] M. Costantini, M. Möller, J. Zachhuber
   *diffstrata -- a Sage package for calculations in the tautological ring of the moduli space of Abelian differentials*
   `arXiv:2006.12815 [math.AG] <arXiv:2006.12815 [math.AG]>`_

.. [CoMoZa-b] M. Costantini, M. Möller, J. Zachhuber
   *The Chern classes and the Euler characteristic of the moduli spaces of abelian differentials*
   `arXiv:2006.12803 [math.AG] <https://arxiv.org/abs/2006.12803>`_

.. [DeScvZ] V. Delecroix, J. Schmitt, J. van Zelm
   *"admcycles - a {S}age package for calculations in the tautological ring of the moduli space of stable curves"*
   `arXiv:2002.01709 [math.AG] <https://arxiv.org/abs/2002.01709>`_

.. [Fa99] C. Faber
   *A conjectural description of the tautological ring of the moduli
   space of curves*
   in *Moduli of curves and abelian varieties*, Aspects Math.,
   E33, Vieweg, Braunschweig (1999), 109-129.

.. [FaPa00] C. Faber and R. Pandharipande
   *Logarithmic series and Hodge integrals in the tautological ring. With
   an appendix by Don Zagier*
   Dedicated to William Fulton on the occasion of his 60th birthday.
   Michigan Math. J. 48 (2000), 215-252.

.. [FaPa05] C. Faber and R. Pandharipande
   *Relative maps and tautological classes*
   J. Eur. Math. Soc. 7 (2005), no. 1, 13-49.

.. [FaPa18] G. Farkas and R. Pandharipande
   *The moduli space of twisted canonical divisors*
   J. Inst. Math. Jussieu 17 (2018), no. 3, 615–672.

.. [GrVa01] T. Graber and R. Vakil,
   *On the tautological ring of Mbar_g,n*
   Turkish J. Math. 25 (2001), no. 1, 237-243.

.. [GrVa05] T. Graber and R. Vakil
   *Relative virtual localization and vanishing of tautological classes on
   moduli spaces of curves*
   Duke Math. J. 130 (2005), no. 1, 1-37.

.. [HoPiSc19] D. Holmes, A. Pixton, J. Schmitt
   *Multiplicativity of the double ramification cycle*
   `Documenta Mathematica, 24. (2019) <https://www.elibm.org/article/10011958>`_

.. [HoSc] D. Holmes and J. Schmitt
   *Infinitesimal structure of the pluricanonical double ramification locus*
   `arXiv:1909.11981 [math.AG] <https://arxiv.org/abs/1909.11981>`_

.. [Io02] E.-N. Ionel
   *Topological recursive relations in H^(2g)(M_g,n)*
   Invent. Math. 148 (2002), no. 3, 627–658.

.. [JPPZ17] F. Janda, R. Pandharipande, A. Pixton, D. Zvonkine
   *Double ramification cycles on the moduli spaces of curves*
   Publ. math. IHES 125 (2017), 221–266.

.. [Lo95] E. Looijenga
   *On the tautological ring of Mg*
   Invent. Math. 121 (1995), no. 2, 411-419.

.. [Mu83] D. Mumford
   *Towards an enumerative geometry of the moduli space of curves*
   Arithmetic and geometry, Pap. dedic. I. R. Shafarevich, Vol. II: Geometry,
   Prog. Math. 36, 271-328 (1983).

.. [Nor] P. Norbury
   *A new cohomology class on the moduli space of curves*
   `arXiv:1712.03662 [math.AG] <https://arxiv.org/abs/1712.03662>`_

.. [OwSo] B. Owens, S. Somerstep
   *Boundary Expression for Chern Classes of the Hodge Bundle on Spaces of Cyclic Covers*
   `arXiv:1912.07720 [math.AG] <https://arxiv.org/abs/1912.07720>`_

.. [PRvZ20] N. Pagani, A. Ricolfi, J. van Zelm
   *Pullbacks of universal Brill-Noether classes via Abel-Jacobi morphisms*
   Trans. Amer. Math. Soc. 372 (2020), no. 7, 4851–4887.

.. [PPZ15] R. Pandharipande, A. Pixton, D. Zvonkine
   *Relations on \bar M_{g,n} via 3-spin structures*
   J. Amer. Math. Soc. 23 (2015), no. 1, 279–309.

.. [Sch18] J. Schmitt
   *Dimension theory of the moduli space of twisted k-differentials*
   Doc. Math. 23 (2018), 871–894.
